var service = 'service';

$(function() {
    const test = window.matchMedia('(max-width: 600px)');
    if (test.matches) {
        $('.collapse-sm').removeClass('show');
    }

    $('#serviceType').selectric().change(function(e) {
        service = e.target.value;
    });

    $('#request-form').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            $.get('https://api.punkapi.com/v2/beers', function(beers) {
                const beersRow = [];
                const beersOdd = [];
                beers.forEach(function(beer, i) {
                    if (i < 5) {
                        beersRow.push(makeBeer(beer));
                    }
                    if (i >= Math.floor(beers.length / 2) && beer.id % 2 > 0) {
                        beersOdd.push(makeBeer(beer));
                    }
                });
                $('#beers-row .beers').empty().append(beersRow);
                $('#beers-odd .beers').empty().append(beersOdd);
                $('#modal-header').text('You requested ' + service + ', we provide beer! This is the way of the Republic.');
                $('#response-modal').modal('show');
            });
        }
    });

});

function makeBeer(beer) {
    const beerDiv = $('<div class="beer"/>');
    beerDiv.append($('<div class="beer-name"><span>' + beer.id + ':</span> ' + beer.name + '</div>'));
    beerDiv.append($('<div class="beer-tagline">' + beer.tagline + '</div>'));
    return beerDiv;
}

